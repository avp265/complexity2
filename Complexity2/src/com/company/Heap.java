package com.company;

public class Heap {

    public int[] minMaxHeap;
    private int currentSize;
    /**
     * Constructor
     * @param numberOfElements: the maximum number the min-max heap can have
     **/
    public Heap(int numberOfElements) {
        minMaxHeap = new int[numberOfElements];
        currentSize = 0;
    }

    /**
     * Calculate the level of i based on the format:
     * Math floor of 2 log (i + 1)
     * @param index: the current position
     * */
    private int levelOf(int index) {
        int base = 2;

        return (int) Math.floor(Math.log(index + 1) / Math.log(base));
    }

    /**
     * Print the state of the min-max heap
     * @param heap: parameter to cast the min-max heap
     * */
    public void print(int[] heap) {
        System.out.println();
        for (int i = 0; i < heap.length; i++) {
            if(heap[i] == 0) {
                break;
            }
            else {
                if(currentSize == 1 && i == 0) {
                    System.out.println("Root Node (" + i + ") : " + heap[i]);
                }
                if(leftChild(i) == heap.length) {
                    break;
                }
                if(heap[leftChild(i)] != 0) {
                    if(i == 0) {
                        System.out.print("Root Node (" + i + ") : " + heap[i] + " | " + "Left child (" + leftChild(i) +"): " + heap[leftChild(i)]);
                    }
                    else {
                        System.out.print("Node (" + i + ") : " + heap[i] + " | " + "Left child (" + leftChild(i) +"): " + heap[leftChild(i)]);
                    }
                    if (rightChild(i) == heap.length) {
                        break;
                    }
                    if(heap[rightChild(i)] != 0) {
                        System.out.print(" | Right child (" + rightChild(i) + "): " + heap[rightChild(i)]);
                    }
                }
            }
            System.out.println();
        }
        System.out.println("The key with minimum priority: " + getLow());
        System.out.println("The key with maximum priority: " + getHigh(heap));
        System.out.println("---------------------------------");
    }

    /**
     * Building the heap based on the Floyd's build heap algorithm
     * @param heap: the min max heap
     * */
    public void buildHeap(int[] heap) {
        int start = (int) Math.floor(heap.length / 2);
        for (int i = start; i > 0 ; i--) {
            pushDown(heap, i);
        }
    }

    /**
     * Trickle down
     * @param heap: the min-max heap
     * @param index: the current position
     * */
    private void pushDown(int[] heap, int index) {
        // if i is in the min level, then i's level is an even level
        if(levelOf(index) % 2 == 0) {
            pushDownMin(heap, index);
        }
        else {
            pushDownMax(heap, index);
        }
    }

    /**
     * Swap values between two positions
     * @param heap: the min-max heap
     * @param first: the first position
     * @param second: the second position
     * */
    public void swap(int[] heap, int first, int second) {
        int temp = heap[first];
        heap[first] = heap[second];
        heap[second] = temp;
    }

    /**
     * Trickle down min operates comparisons between i and its smallest child or grandchild.
     * And swap values between them
     * @param heap: the min-max heap
     * @param index: the current position
     * */
    private void pushDownMin(int[] heap, int index) {
        // To prevent the example case that smallest child on the right and the smallest grand child on the left
        // We cannot apply the same method smallestChild to the position of smallest child
        if(hasChildren(index)) {
            // min is the smallest child or grandchild of node index
            int min = smallest(heap, index);
            // if min is the position of index's grandchild, then min has to bigger than index's right child
            if(min > rightChild(index)) {
                // compare min's value to its grandparent
                if(heap[min] < heap[index]) {
                    // then swap
                    swap(heap, min, index);
                    // if the smallest grandchild's value > its parent's value
                    if(heap[min] > heap[parent(min)]) {
                        // swap smallest grand child with its parent
                        swap(heap, min, parent(min));
                    }
                    pushDownMin(heap, min);
                }
            }
            else if(heap[min] < heap[index]) {
                swap(heap, min, index);
            }
        }
    }
    /**
     * Trickle down max operates comparisons between i and its biggest child or grandchild.
     * And swap values between them
     * @param heap: the min-max heap
     * @param index: the current position
     * */
    private void pushDownMax(int[] heap, int index) {
        // if i (index) has children then
        if(hasChildren(index)) {
            // max is the biggest child or grandchild of i
            int max = biggest(heap, index);
            // if max is biggest grandchild's position, then it has to be bigger than its grandparent's right child
            if(max > rightChild(index)) {
                // compare values between max and its grandparent
                if (heap[max] > heap[index]) {
                    //swap values
                    swap(heap, max, index);
                    // compare again, but with max's parent
                    if (heap[max] < heap[parent(max)]) {
                        // swap again
                        swap(heap, max, parent(max));
                    }
                    pushDownMax(heap, max);
                }
                // compare values between max and its grandparent's biggest child
            } else if(heap[max] > heap[index]) {
                swap(heap, max, index);
            }
        }
    }

    /**
     * Insertion process (bubble up or push-up)
     * @param heap: the min-max heap
     * @param index: the last position (getCurrentSize())
     * @param value: the value to add (value > 0, if == 0, then the heap still has free position)
     * */
    public void add(int[] heap, int index, int value) {
        // root node is set at index 0
        // checking x is not at the root node by comparing values of x and root node
        System.out.println("Add: " + value);
        heap[index] = value;
        currentSize++;
        // if i (index) is on a min level
        if(levelOf(index) % 2 == 0) {
            // compare values between i and its parents
            if(heap[index] > heap[parent(index)]) {
                // swap values
                swap(heap, index, parent(index));
                // bubble up max with i's parents
                pushUpMax(heap, parent(index));
            }
            else {
                // bubble up min with i
                pushUpMin(heap, index);
            }
        }
        else {
            // compare values between i and its parents
            if(heap[index] < heap[parent(index)]) {
                // swap values
                swap(heap, index, parent(index));
                // bubble up min
                pushUpMin(heap, parent(index));
            }
            else {
                // bubble up max
                pushUpMax(heap, index);
            }
        }
    }

    /**
     * Bubble up min
     * @param heap: the min-max heap
     * @param index: the current position to cast
     * */
    private void pushUpMin(int[] heap, int index) {
        if(levelOf(index) > 1) {
            // i has grandparent when i located at level 2 above
            // compare values between i and its grandparent then swap
            if (heap[grandParent(index)] != 0 && heap[index] < heap[grandParent(index)]) {
                swap(heap, index, grandParent(index));
                // bubble up min i's grandparent
                pushUpMin(heap, grandParent(index));
            }
        }
    }

    /**
     * Bubble up max
     * @param heap: the min-max heap
     * @param index: the current position to cast
     * */
    private void pushUpMax(int[] heap, int index) {
        if(levelOf(index) > 1) {
            // apply similarly from bubble up min to max
            if(heap[grandParent(index)] != 0 && heap[index] > heap[grandParent(index)]) {
                swap(heap, index, grandParent(index));
                pushUpMax(heap, grandParent(index));
            }
        }
    }

    /**
     * Return the position of parent of the current position
     * @param index: the current position
     * */
    public int parent(int index) {
        return (index - 1) / 2;
    }

    /**
     * Return the left child's position of i
     * @param index: the current position
     * */
    public int leftChild(int index) {
        return (2 * index) + 1;
    }

    /**
     * Return the right child's position of the current position
     * @param index: the current position
     * */
    public int rightChild(int index) {
        return (2 * index) + 2;
    }

    /**
     * Return the grandparent's position
     * @param x: the current position
     * */
    public int grandParent(int x) {
        return parent(parent(x));
    }

    /**
     * If the heap's current size is 0, then return true
     * */
    public boolean isEmpty() {
        return currentSize == 0;
    }

    /**
     * Return the current number of elements in the min-max heap
     *
     * */
    public int getCurrentSize() {
        return currentSize;
    }

    /**
     * Return the root node
     * */
    public int getLow() {
        return minMaxHeap[0];
    }
    /**
     * Return the biggest child of the root node
     * */
    public int getHigh(int[] heap) {
        return heap[biggestChild(heap, 0)];
    }

    /**
     * Remove the root node
     * @param heap: the min-max heap
     * */
    public void removeLow(int[] heap) {
        System.out.println("Remove " + heap[0]);
        // swap values from the last element to the root
        heap[0] = heap[getCurrentSize() - 1];
        // erase value of the last node
        heap[getCurrentSize() - 1] = 0;
        // reduce current size of the heap
        currentSize--;
        // invoke trickle down on the root node
        pushDown(heap, 0);
    }

    /**
     * Remove the biggest child of the root node
     * @param heap: the min-max heap
     * */
    public void removeHigh(int[] heap) {
        // swap values from the last node to the biggest node
        int high = biggestChild(heap, 0);
        System.out.println("Remove " + heap[high]);
        heap[high] = heap[getCurrentSize() - 1];
        // erase value of the last node
        heap[getCurrentSize() - 1] = 0;
        // decrease the current size
        currentSize--;
        // trickle down on the biggest node
        pushDown(heap, high);
    }

    /**
     * Return true if the current position has 1 or 2 children
     * @param index: the current position
     * */
    private boolean hasChildren(int index) {
        // 2 cases of i having 1 or 2 children, also restraint within the current size
        return  rightChild(index) < currentSize || leftChild(index) < currentSize;
    }

    /**
     * Return the position of the smallest child of the current position
     * @param heap: the min-max heap
     * @param index: the current position
     * */
    public int smallestChild(int[] heap, int index) {
        int smallest = 0;
        // we only find out when i has children
        if(hasChildren(index)) {
            // assume i's left child is smallest, but still in the bound of size
            if(leftChild(index) < currentSize) {
                smallest = leftChild(index);
            }
            // now if i's right child is smaller than i's left child, then smallest child will be changed
            if (heap[rightChild(index)] < heap[leftChild(index)] && rightChild(index) < currentSize && rightChild(index) > leftChild(index)) {
                smallest = rightChild(index);
            }
        }
        return smallest;
    }

    /**
     * Return the position of the smallest grandchild of the current position
     * @param heap: the min max heap
     * @param index: the current position
     * */
    public int smallestGrandChild(int[] heap, int index) {
        // each node will have 4 grandchildren, so we find the smallest representatives of i's children (left and right)
        int smallestLeftGrandChild = smallestChild(heap, leftChild(index));
        int smallestRightGrandChild = smallestChild(heap, rightChild(index));
        int smallest = 0;
        // assume result will be i's smallest left grandchild
        if(smallestLeftGrandChild < currentSize && smallestLeftGrandChild > rightChild(index)) {
            smallest = smallestLeftGrandChild;
        }
        // if i's smallest right grandchild is smaller, then change the smallest value
        // also prevent the case that i has only smallest left grandchild and no smallest right grandchild by the comparison of positions,
        // but still within the current size, because the result may return 0 which is root node, that's not correct.
        if (heap[smallestLeftGrandChild] > heap[smallestRightGrandChild] && smallestRightGrandChild < currentSize && smallestRightGrandChild > smallestLeftGrandChild) {
            smallest = smallestRightGrandChild;
        }
        return smallest;
    }

    /**
     * Return the position of the smallest child or grandchild of the current position
     * @param heap: the min max heap
     * @param index: the current position
     * */
    public int smallest(int[] heap, int index) {
        int smallest = 0;
        int smallestChild = smallestChild(heap, index);
        int smallestGrandChild = smallestGrandChild(heap, index);
        // assume smallest now is smallest child, but still within the size
        if(smallestChild < currentSize) {
            smallest = smallestChild;
        }
        // if i's smallest grandchild is smaller, then smallest will be changed
        // also prevent the case that i has only smallest child and no smallest grandchild by the comparison of positions,
        // but still within the current size, because the result may return 0 which is root node, that's not correct.
        if(smallestGrandChild < currentSize && smallestGrandChild > smallestChild && heap[smallestGrandChild] < heap[smallestChild] ) {
            smallest = smallestGrandChild;
        }
        return smallest;
    }

    /**
     * Return the position of the biggest child of the current position
     * @param heap: the min max heap
     * @param index: the current position
     * */
    public int biggestChild(int[] heap, int index) {

        int biggestChild = 0;
        if(hasChildren(index)) {
            // checking if i has any children
            // assume i has the left child
            if (leftChild(index) < currentSize) {
                biggestChild = leftChild(index);
            }
            // the position of the right child is always larger than the left, we need to check and compare their values to determine the biggest child, but still within the current size
            if (rightChild(index) < currentSize && heap[rightChild(index)] > heap[leftChild(index)] && rightChild(index) > leftChild(index)) {
                biggestChild = rightChild(index);
            }
        }
        return biggestChild;
    }

    /**
     * Return the position of the biggest grandchild of the current position
     * @param heap: the min max heap
     * @param index: the current position
     * */
    public int biggestGrandChild(int[] heap, int index) {
        int biggestLeft = biggestChild(heap, leftChild(index));
        int biggestRight = biggestChild(heap, rightChild(index));

        int biggestGrandChild = 0;
        // assume we have a biggest child on the left
        if(biggestLeft < currentSize) {
            biggestGrandChild = biggestLeft;
        }
        // position of the biggest child on the right is larger than the left to prevent the case returns 0
        // if the values of the right one is bigger than the left one, then return the right one
        if(heap[biggestLeft] < heap[biggestRight] && biggestRight < currentSize && biggestRight > biggestLeft) {
            biggestGrandChild = biggestRight;
        }
        return biggestGrandChild;
    }
    /**
     * Return the position of the biggest child or grandchild of the current position
     * @param heap: the min max heap
     * @param index: the current position
     * */
    public int biggest(int[] heap, int index) {
        int biggest = 0;
        int biggestChild = biggestChild(heap, index);
        int biggestGrandChild = biggestGrandChild(heap, index);
        // assume we have the biggest as the biggest child
        if(biggestChild < currentSize) {
            biggest = biggestChild;
        }
        // if the biggest grandchild's value is bigger than the biggest child's value, and also compare the position to prevent the case returns 0, but still within the curren size
        if(heap[biggestGrandChild] > heap[biggestChild] && biggestGrandChild < currentSize && biggestGrandChild > biggestChild) {
            biggest = biggestGrandChild;
        }
        return biggest;
    }
}
