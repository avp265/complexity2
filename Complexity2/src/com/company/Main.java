package com.company;

public class Main {

    public static void main(String[] args) {
        new Main().run();
    }

    public void run() {

        int[] sampleInputs = {1, 17, 16, 9, 39, 19, 35, 25, 20, 21};
        Heap heap = new Heap(15);
        System.out.println();
        System.out.println("Min-max Heap:\n");

        // the min-max heap is empty then add new values from the sample inputs
        if(heap.isEmpty()) {
            for (int i = 0; i < sampleInputs.length; i++) {
                // add new value and also print the current state of the min max heap
                heap.add(heap.minMaxHeap, i, sampleInputs[i]);
                heap.print(heap.minMaxHeap);
            }
        }

        heap.buildHeap(heap.minMaxHeap);
        // add 40
        heap.add(heap.minMaxHeap, heap.getCurrentSize(), 40 );
        heap.print(heap.minMaxHeap);

        // remove the minimum
        heap.removeLow(heap.minMaxHeap);
        heap.print(heap.minMaxHeap);

        // remove the maximum
        heap.removeHigh(heap.minMaxHeap);
        heap.print(heap.minMaxHeap);

        // add 3
        heap.add(heap.minMaxHeap, heap.getCurrentSize(), 3 );
        heap.print(heap.minMaxHeap);

        // remove the maximum
        heap.removeHigh(heap.minMaxHeap);
        heap.print(heap.minMaxHeap);

        /**
         *    Below is the sample data structure, we use to build the min-max heap.
         *
         *    Sample from inputs: {1, 17, 16, 9, 39, 19, 35, 25, 20}
         *
         *    ------------------------------------------------
         *
         *    Ideal result:
         *
         *    Root node: 1 | Left child: 39 | Right child: 35
         *
         *    Node: 39 | Left child: 17 | Right child: 16
         *
         *    Node: 35 | Left child: 9 | Right child: 19
         *
         *    Node: 17 | Left child: 25 | Right child: 20
         *
         *    ------------------------------------------------
         *
         *    In binary tree:
         *
         *                  1           min level -> 0
         *                /   \
         *              39     35       max level -> 1
         *            /  \    /  \
         *          17   16  9   19     min level -> 2
         *         / \
         *        25 20                 max level -> 3
         *
         *    ------------------------------------------------
         *
         *    In array:
         *
         *    Heap -> [1, 39, 35, 17, 16, 9, 19, 25, 20]
         *
         *   Index -> 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8
         **/

    }
}
